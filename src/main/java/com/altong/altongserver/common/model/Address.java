package com.altong.altongserver.common.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    @Column(name = "zip_code")
    private String zipCode;
    @Column(name = "road_name_address")
    private String roadNameAddress;
    @Column(name = "detail_address")
    private String detailAddress;
}
