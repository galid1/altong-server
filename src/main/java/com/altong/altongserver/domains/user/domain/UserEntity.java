package com.altong.altongserver.domains.user.domain;

import com.altong.altongserver.common.logging.BaseTimeEntity;
import com.altong.altongserver.common.model.Money;
import com.altong.altongserver.domains.user.domain.vo.UserInformation;
import com.altong.altongserver.domains.user_work_place.domain.UserWorkPlaceEntity;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Table(name = "users")
public class UserEntity extends BaseTimeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long userId;

    @Embedded
    @NonNull
    private UserInformation userInformation;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "current_salary"))
    private Money currentSalary = new Money(0);

    @ElementCollection
    @CollectionTable(name = "user_user_work_place_id", joinColumns = @JoinColumn(name = "user_id"))
    private List<Long> userAndUserWorkPlaceIdList = new ArrayList<>();

    @Builder
    public UserEntity (@NonNull UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    public void addWorkPlace(Long workPlaceId) {
        this.userAndUserWorkPlaceIdList.add(workPlaceId);
    }

    // 현재 급여 최신화
    public void updateCurrentSalary(Money newSalary) {

    }

    // 출금 (출금전 계좌로 돈을 보내기 위해서는 계좌 등록이 완료된 상태여야함)


}
