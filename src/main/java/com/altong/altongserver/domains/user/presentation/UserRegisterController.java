package com.altong.altongserver.domains.user.presentation;

import com.altong.altongserver.domains.user.presentation.request_response.UserRegisterRequest;
import com.altong.altongserver.domains.user.presentation.request_response.UserRegisterResponse;
import com.altong.altongserver.domains.user.service.UserRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserRegisterController {
    @Autowired
    private UserRegisterService userRegisterService;

    @PostMapping("/")
    public UserRegisterResponse signUp(@Valid @RequestBody UserRegisterRequest userRegisterRequest) {
        return userRegisterService.registerUser(userRegisterRequest);
    }
}
