package com.altong.altongserver.domains.user.service;

import com.altong.altongserver.domains.user.domain.UserEntity;
import com.altong.altongserver.domains.user.domain.UserRepository;
import com.altong.altongserver.domains.user.presentation.request_response.AddWorkPlaceRequest;
import com.altong.altongserver.domains.user.presentation.request_response.AddWorkPlaceResponse;
import com.altong.altongserver.domains.user_work_place.domain.UserWorkPlaceEntity;
import com.altong.altongserver.domains.user_work_place.domain.UserWorkPlaceRepository;
import com.altong.altongserver.domains.user_work_place.domain.vo.EmploymentContract;
import com.altong.altongserver.domains.work_place.domain.WorkPlaceEntity;
import com.altong.altongserver.domains.work_place.domain.WorkPlaceRepository;
import com.altong.altongserver.domains.work_place.domain.vo.WorkPlaceInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserAddWorkPlaceService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WorkPlaceRepository workPlaceRepository;
    @Autowired
    private UserWorkPlaceRepository userWorkPlaceRepository;

    @Transactional
    public AddWorkPlaceResponse addWorkPlace(Long userId, AddWorkPlaceRequest addWorkPlaceRequest) {
        WorkPlaceEntity workPlaceEntity = addWorkPlaceEntity(addWorkPlaceRequest.getEmploymentContract().getBusinessNum(),
                addWorkPlaceRequest.getWorkPlaceInformation());

        UserEntity userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 사용자입니다."));

        addUserWorkPlaceEntity(userEntity, workPlaceEntity, addWorkPlaceRequest.getEmploymentContract());

        return AddWorkPlaceResponse.builder()
                .userId(userId)
                .workPlaceId(workPlaceEntity.getWorkPlaceId())
                .build();
    }

    // 근무지 추가 메소드
    private WorkPlaceEntity addWorkPlaceEntity(String businessNum, WorkPlaceInformation workPlaceInformation) {
        WorkPlaceEntity newWorkPlace = WorkPlaceEntity.builder()
                .businessNum(businessNum)
                .workPlaceInformation(workPlaceInformation)
                .build();

        return workPlaceRepository.save(newWorkPlace);
    }

    // 사용자_근무지 엔티티 추가 메소드
    private void addUserWorkPlaceEntity(UserEntity user, WorkPlaceEntity workPlace, EmploymentContract employmentContract) {
        UserWorkPlaceEntity newUserWorkPlace = UserWorkPlaceEntity.builder()
                .user(user)
                .workPlace(workPlace)
                .employmentContract(employmentContract)
                .build();

        userWorkPlaceRepository.save(newUserWorkPlace);
    }
}
