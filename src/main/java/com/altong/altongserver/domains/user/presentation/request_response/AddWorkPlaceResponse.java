package com.altong.altongserver.domains.user.presentation.request_response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddWorkPlaceResponse {
    private Long userId;
    private Long workPlaceId;
}
