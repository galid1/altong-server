package com.altong.altongserver.domains.user.presentation.request_response;

import com.altong.altongserver.domains.user_work_place.domain.vo.EmploymentContract;
import com.altong.altongserver.domains.work_place.domain.vo.WorkPlaceInformation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddWorkPlaceRequest {
    private WorkPlaceInformation workPlaceInformation;
    private EmploymentContract employmentContract;
}
