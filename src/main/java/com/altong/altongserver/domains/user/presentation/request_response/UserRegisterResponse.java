package com.altong.altongserver.domains.user.presentation.request_response;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRegisterResponse {
    private Long userId;
}
