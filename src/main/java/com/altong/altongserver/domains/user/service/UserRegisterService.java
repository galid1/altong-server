package com.altong.altongserver.domains.user.service;

import com.altong.altongserver.domains.user.domain.UserRepository;
import com.altong.altongserver.domains.user.presentation.request_response.UserRegisterRequest;
import com.altong.altongserver.domains.user.presentation.request_response.UserRegisterResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserRegisterService {
    @Autowired
    private UserRepository userRepository;

    @Transactional
    public UserRegisterResponse registerUser(UserRegisterRequest userRegisterRequest) {
        Long userId = userRepository.save(userRegisterRequest.toEntity()).getUserId();
        return UserRegisterResponse.builder()
                .userId(userId)
                .build();
    }
}
