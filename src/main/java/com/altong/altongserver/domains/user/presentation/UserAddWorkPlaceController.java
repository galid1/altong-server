package com.altong.altongserver.domains.user.presentation;

import com.altong.altongserver.domains.user.presentation.request_response.AddWorkPlaceRequest;
import com.altong.altongserver.domains.user.presentation.request_response.AddWorkPlaceResponse;
import com.altong.altongserver.domains.user.service.UserAddWorkPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserAddWorkPlaceController {
    @Autowired
    private UserAddWorkPlaceService addWorkPlaceService;

    @PostMapping("/{usersId}/workPlaces")
    public AddWorkPlaceResponse addWorkPlaces(@PathVariable("usersId") Long userId, @RequestBody AddWorkPlaceRequest addWorkPlaceRequest) {
        return addWorkPlaceService.addWorkPlace(userId, addWorkPlaceRequest);
    }

}
