package com.altong.altongserver.domains.user.domain.vo;

import com.altong.altongserver.domains.user.presentation.valid.AccountValidAnnotation;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Embeddable
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Account {
    private String bank;
    @Column(name = "account_num")
    @AccountValidAnnotation
    private String accountNum;
}
