package com.altong.altongserver.domains.user.presentation.valid;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AccountValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AccountValidAnnotation {
    String message() default "계좌번호를 확인하세요.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
