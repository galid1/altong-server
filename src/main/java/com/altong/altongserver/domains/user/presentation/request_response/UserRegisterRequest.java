package com.altong.altongserver.domains.user.presentation.request_response;

import com.altong.altongserver.domains.user.domain.UserEntity;
import com.altong.altongserver.domains.user.domain.vo.UserInformation;
import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRegisterRequest {
    @NotNull
    private UserInformation userInformation;

    public UserEntity toEntity() {
        return UserEntity.builder()
                .userInformation(userInformation)
                .build();
    }
}
