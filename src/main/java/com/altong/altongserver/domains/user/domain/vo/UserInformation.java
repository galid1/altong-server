package com.altong.altongserver.domains.user.domain.vo;

import com.altong.altongserver.domains.user.domain.enums.Gender;
import com.altong.altongserver.domains.user.presentation.valid.AccountValidAnnotation;
import com.altong.altongserver.domains.user.presentation.valid.BirthDayValidAnnotation;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Embeddable
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserInformation {
    @Column(name = "user_name")
    @NotNull
    private String userName;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Gender gender;

    @Column(name = "birth_day")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @BirthDayValidAnnotation
    @NotNull
    private LocalDate birthDay;

    @Embedded
    @AccountValidAnnotation
    @NotNull
    private Account account;

    @Builder
    public UserInformation(String userName, Gender gender, LocalDate birthDay, Account account) {
        this.userName = userName;
        this.gender = gender;
        this.birthDay = birthDay;
        this.account = account;
    }

}
