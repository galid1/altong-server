package com.altong.altongserver.domains.user.presentation.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class BirthDayValidator implements ConstraintValidator<BirthDayValidAnnotation, LocalDate> {

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        return verifyNotBaby(value) && value.toString().length() == 10;
    }

    // 법적나이 만 13세 이하는 부모님의 동의가 필요함 따라서 일단은 가입이 안되도록함.
    private boolean verifyNotBaby(LocalDate value) {
        LocalDate now = LocalDate.now();
        int diff = now.getYear() - value.getYear();
        return diff >= 13;
    }

}
