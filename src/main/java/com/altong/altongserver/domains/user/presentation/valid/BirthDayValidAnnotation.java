package com.altong.altongserver.domains.user.presentation.valid;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = BirthDayValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface BirthDayValidAnnotation {
    String message() default "만 13세 이하는 가입이 불가능합니다. 또는 날짜 입력형식을 맞추어 주세요.(yyyy-MM-dd)";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
