package com.altong.altongserver.domains.user.domain.enums;

public enum Gender {
    MALE, FEMALE
}
