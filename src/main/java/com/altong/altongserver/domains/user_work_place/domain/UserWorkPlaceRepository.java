package com.altong.altongserver.domains.user_work_place.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserWorkPlaceRepository extends JpaRepository<UserWorkPlaceEntity, UserWorkPlaceId> {
}
