package com.altong.altongserver.domains.user_work_place.domain.vo;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDate;

@Embeddable
@Getter
@NoArgsConstructor
public class ContractPeriod {
    @Column(name = "contract_start_period")
    private LocalDate contractStartPeriod;

    @Column(name = "contract_end_period")
    private LocalDate contractEndPeriod;

    @Builder
    public ContractPeriod(LocalDate contractStartPeriod, LocalDate contractEndPeriod) {
        this.contractStartPeriod = contractStartPeriod;
        this.contractEndPeriod = contractEndPeriod;
    }
}
