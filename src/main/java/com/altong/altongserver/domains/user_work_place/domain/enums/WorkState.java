package com.altong.altongserver.domains.user_work_place.domain.enums;

public enum WorkState {
    //재직중      재직중이 아닌
    IN_OFFICE, OUT_OF_OFFICE
}
