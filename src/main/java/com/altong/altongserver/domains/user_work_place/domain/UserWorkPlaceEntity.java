package com.altong.altongserver.domains.user_work_place.domain;

import com.altong.altongserver.domains.user.domain.UserEntity;
import com.altong.altongserver.domains.user_work_place.domain.enums.WorkState;
import com.altong.altongserver.domains.user_work_place.domain.vo.EmploymentContract;
import com.altong.altongserver.domains.work_place.domain.WorkPlaceEntity;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "user_work_place",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"user_id", "business_num"})
    })
@IdClass(UserWorkPlaceId.class)
@NoArgsConstructor
public class UserWorkPlaceEntity {
    @Id
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Id
    @ManyToOne
    @JoinColumn(name = "work_place_id")
    private WorkPlaceEntity workPlace;

    // 재직 상태
    @Embedded
    private WorkState workState = WorkState.OUT_OF_OFFICE;

    @Embedded
    private EmploymentContract employmentContract;

    @Builder
    public UserWorkPlaceEntity(UserEntity user, WorkPlaceEntity workPlace, EmploymentContract employmentContract) {
        this.user = user;
        this.workPlace = workPlace;
        this.workState = WorkState.OUT_OF_OFFICE;
        this.employmentContract = employmentContract;
    }

    public void toInOffice() {
        verifyCanInOffice();
        this.workState = WorkState.IN_OFFICE;
    }

    private void verifyCanInOffice() {

    }
}
