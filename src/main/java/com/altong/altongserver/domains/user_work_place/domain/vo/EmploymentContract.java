package com.altong.altongserver.domains.user_work_place.domain.vo;

import com.altong.altongserver.common.model.Money;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Getter
@Embeddable
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentContract {
    private String employee;

    @Column(name = "business_num")
    private String businessNum;

    @Embedded
    private WorkingHours workingHours;

    @Embedded
    private ContractPeriod contractPeriod;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "hourly_wage"))
    private Money hourlyWage;
}
