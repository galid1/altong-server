package com.altong.altongserver.domains.user_work_place.domain.vo;

import com.altong.altongserver.domains.work_place.domain.enums.WorkingDay;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Embeddable
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkingHours {
    @Enumerated(EnumType.STRING)
    @NotNull
    private WorkingDay workingDay;

    @Column(name = "working_start_time")
    @NotNull
    private Integer workingStartTime;

    @Column(name = "working_end_time")
    @NotNull
    private Integer workingEndTime;
}
