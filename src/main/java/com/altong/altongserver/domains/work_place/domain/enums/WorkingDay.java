package com.altong.altongserver.domains.work_place.domain.enums;

public enum WorkingDay {
    //  평일      매일      주말         월 ~ 토
    WEEK_DAY, EVERY_DAY, WEEK_END, MONDAY_TO_SATURDAY
}
