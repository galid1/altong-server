package com.altong.altongserver.domains.work_place.domain.vo;

import com.altong.altongserver.common.model.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.constraints.NotNull;

@Getter
@Embeddable
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkPlaceInformation {
    @Column(name = "work_place_name")
    @NotNull
    private String workPlaceName;

    @Embedded
    @NotNull
    private Address workPlaceAddress;
}
