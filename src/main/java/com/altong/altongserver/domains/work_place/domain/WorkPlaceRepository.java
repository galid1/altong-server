package com.altong.altongserver.domains.work_place.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkPlaceRepository extends JpaRepository<WorkPlaceEntity, Long> {
}
