package com.altong.altongserver.domains.work_place.domain;

import com.altong.altongserver.domains.work_place.domain.vo.WorkPlaceInformation;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;

@Embeddable
@Getter
@NoArgsConstructor
@Entity
@Table(name = "work_place")
public class WorkPlaceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long workPlaceId;

    @Column(name = "business_num", unique = true)
    private String businessNum;

    @Embedded
    @NonNull
    private WorkPlaceInformation workPlaceInformation;

    @Builder
    public WorkPlaceEntity(@NonNull WorkPlaceInformation workPlaceInformation, String businessNum) {
        this.workPlaceInformation = workPlaceInformation;
        this.businessNum = businessNum;
    }
}

